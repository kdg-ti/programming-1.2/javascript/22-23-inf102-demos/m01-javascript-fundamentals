const BLACK = "#";
const WHITE = ".";
const SIZE = 8;
let even = "";
let uneven = "";
for (let i = 0; i < SIZE; i++) {
  if (i % 2 === 0) {
    uneven += BLACK;
    even += WHITE;
  } else {
    uneven += WHITE;
    even += BLACK;
  }
}
for (let i = 0; i < SIZE; i++) {
  if (i % 2 === 0) {
    console.log(even);

  } else {
    console.log(uneven);
  }
}

